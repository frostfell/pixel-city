//
//  Constants.swift
//  pixel-city
//
//  Created by Igor Chernyshov on 26.11.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import Foundation

let APIKey = "d7845523c70024fedd164176f120d051"

func flickrURL(forAPIKey key: String, withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String {
    // Returns a finished URL that requests photos just as we need it
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(APIKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=km&per_page=\(number)&format=json&nojsoncallback=1"
}


