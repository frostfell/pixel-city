//
//  PhotoCell.swift
//  pixel-city
//
//  Created by Igor Chernyshov on 26.11.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell { // Both functions below are required to use a custom UICollecitonViewCell
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
