//
//  DropablePin.swift
//  pixel-city
//
//  Created by Igor Chernyshov on 13.11.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DroppablePin: NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D // This is just the way how it works. We need a coordinate dynamic variable
    var identifier: String                 // And an identifier string variable
    
    init(coodrinate: CLLocationCoordinate2D, identifier: String) {
        self.coordinate = coodrinate
        self.identifier = identifier    // Self is for class, identifier is a "local" passed to a function
        super.init()
    }
}
